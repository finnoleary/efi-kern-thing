OOBJ=loader.o main.o lexer.o
OLIB=bin/kernel.so
OEFI=bin/kernel.efi
OIMG=bin/kernel.img
OHDA=bin/hdimg.bin

build:
	gcc loader.c main.c lexer.c            \
	    -c                                 \
	    -fno-stack-protector               \
	    -fpic                              \
	    -fshort-wchar                      \
	    -mno-red-zone                      \
	    -I /usr/include/efi/               \
	    -I /usr/include/efi/x86_64/        \
	    -DEFI_FUNCTION_WRAPPER

	ld $(OOBJ)                             \
	   /usr/lib/crt0-efi-x86_64.o          \
	   -nostdlib                           \
	   -znocombreloc                       \
	   -T /usr/lib/elf_x86_64_efi.lds      \
	   -shared                             \
	   -Bsymbolic                          \
	   -L /usr/lib/                        \
	   -l:libgnuefi.a                      \
	   -l:libefi.a                         \
	   -o $(OLIB)
	objcopy -j .text                       \
	        -j .sdata                      \
	        -j .data                       \
	        -j .dynamic                    \
	        -j .dynsym                     \
	        -j .rel                        \
	        -j .rela                       \
	        -j .reloc                      \
	        --target=efi-app-x86_64        \
	        $(OLIB)                        \
	        $(OEFI)

load:
	dd if=/dev/zero of=$(OIMG) bs=1k count=1440
	mformat -i $(OIMG) -f 1440 ::
	mmd -i $(OIMG) ::/EFI
	mmd -i $(OIMG) ::/EFI/BOOT
	cp $(OEFI) bin/BOOTX64.EFI
	mcopy -i $(OIMG) bin/BOOTX64.EFI ::/EFI/BOOT
	rm bin/BOOTX64.EFI
	mkgpt -o $(OHDA) --image-size 4096 --part $(OIMG) --type system


test:
	qemu-system-x86_64 -m 3G -bios ~/efi-boot/OVMF.fd -hda $(OHDA)

clean:
	rm -rf bin/*
