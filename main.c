#include "inc/efibind.h"
#include "inc/loader.h"
#include "inc/kernel.h"

struct _loader *loader;
struct _kernel *kernel;

void k_shutdown()
{
    loader->runtime->ResetSystem(EfiResetShutdown, efi_success, 0, NULL);
}

void k_print(char16 *s)
{
    uefi_call_wrapper(loader->system_table->ConOut->OutputString, 2,
                      loader->system_table->ConOut, s);
}

char16 k_readkey()
{
    efi_status state = efi_not_ready;
    char16 key;
    while (state == efi_not_ready)
        state = uefi_call_wrapper(
                loader->system_table->ConIn->ReadKeyStroke, 2,
                loader->system_table->ConIn, &key);
    return key;
}

void k_panic(char16 *s)
{
    k_print(L"\r\nKernel panic: ");
    k_print(s);
    k_print(L"\r\nPress any key to reset.");
    k_readkey();
    k_shutdown();
}

void k_main(struct _loader *ld)
{
    loader = ld;
    k_print(L"Entered kernel.\r\nStarting allocator!\r\n");
    k_salloc();

    k_panic(L"Woo.");
}

void k_memset(void *pt, char v, uint64_t size)
{
    while (size-- > 0)
        *((char*)pt++) = v;
}

void k_salloc()
{
    struct _pool *pt = loader->mem.pool;
    k_memset(pt, 0, sizeof(struct _pool));
    pt->nused++;

    pt->obj[0].start = (char*)pt + sizeof(struct _pool);
    pt->obj[0].size = loader->mem.size - sizeof(struct _pool);
    pt->obj[0].used = 0;
    pt->obj[0].frontier = 1;
}

void *k_alloc(uint64_t size)
{
    k_print(L"boop\r\n");
    /* Find available space
     * If there is a free chunk small enough, and it is not the frontier:
     *      use it.
     *  if it is the frontier:
     *      split it in two and use the first part,
     *      designate the rest as frontier. */
    struct _pool *pt;
    int i;
    for (i = 0; i <= OBJSIZ; i++) {
        if (i == OBJSIZ && !pt->next)
            return NULL; /* we can't find any free space! */

        if (!(!pt->obj[i].used && pt->obj[i].size >= size))
           continue; /* ew ew ew ew speedup ew ew */
        else
        {
            if (!pt->obj[i].frontier) {
                /* We managed to find a free chunk big enough */
                pt->obj[i].used = 1;
                return pt->obj[i].start;
            }
            if (pt->nused == OBJSIZ) {
                void *frontier, *mem;
                uint64_t sz;

                /* frontier = remaining untouched space */
                frontier = pt->obj[i].start;
                sz = pt->obj[i].size;

                /* allocate the next pool record */
                pt->next = frontier;
                frontier += sizeof(struct _pool);
                sz -= sizeof(struct _pool);
                k_memset(pt->next, 0, sizeof(struct _pool));

                /* allocate the requested space */
                mem = frontier;
                frontier += size;
                sz -= size;
                pt->obj[i].start = mem;
                pt->obj[i].size = size;
                pt->obj[i].used = 1;
                pt->obj[i].frontier = 0;
                k_memset(mem, 0, size);

                /* prod the next frontier into existence */
                pt = pt->next;
                pt->nused++;

                pt->obj[0].frontier = 1;
                pt->obj[0].used = 0;
                pt->obj[0].start = frontier;
                pt->obj[0].size = sz;

                return mem;
            }
        }
    }
}

void k_free(void *ptr)
{
    struct _pool *ppt = kernel->pool;
    int i;
    for (i = 0; ppt && i <= OBJSIZ; i++) {
        if (i == OBJSIZ) {
            i = 0;
            ppt = ppt->next;
        }
        if (!ppt->obj[i].used)
            continue;

        if ((char *)ptr >= (char *)ppt->obj[i].start &&
            (char *)ptr <  (char *)(ppt->obj[i].start + ppt->obj[i].size))
            ppt->obj[i].used = 0;
    }
}
