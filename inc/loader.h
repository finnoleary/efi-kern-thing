#define target_width  800
#define target_height 600

struct _loader {
    efi_handle                        image_handle;
    efi_system_table                 *system_table;
    efi_runtime_services             *runtime;
    efi_boot_services                *boot;

    efi_input_key                     key;

    struct {
        uint64_t size;
        void *pool;
    } mem;

    struct {
        efi_graphics_output_protocol *interface;
    } gfx;

};
