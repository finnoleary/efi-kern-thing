#ifndef EFI_BINDINGS
#define EFI_BINDINGS

#include <efi/efi.h>
#include <efi/efilib.h>

#define efiapi                EFIAPI
#define efi_guid              EFI_GUID
#define efi_handle            EFI_HANDLE
#define efi_system_table      EFI_SYSTEM_TABLE
#define efi_runtime_services  EFI_RUNTIME_SERVICES
#define efi_boot_services     EFI_BOOT_SERVICES
#define efi_graphics_output_protocol \
        EFI_GRAPHICS_OUTPUT_PROTOCOL
#define efi_graphics_output_mode_information \
        EFI_GRAPHICS_OUTPUT_MODE_INFORMATION

#define efi_input_key         EFI_INPUT_KEY
#define efi_status            EFI_STATUS

#define efi_success           EFI_SUCCESS
#define efi_not_ready         EFI_NOT_READY
#define efi_out_of_resources  EFI_OUT_OF_RESOURCES
#define efi_buffer_too_small  EFI_BUFFER_TOO_SMALL
#define efi_invalid_parameter EFI_INVALID_PARAMETER
#define efi_not_found         EFI_NOT_FOUND
#define efi_access_denied     EFI_ACCESS_DENIED
#define efi_unsupported       EFI_UNSUPPORTED
#define efi_already_started   EFI_ALREADY_STARTED

#define uintn                 UINTN
#define uint32                UINT32
#define char16                CHAR16
#define efi_memory_descriptor EFI_MEMORY_DESCRIPTOR


/* Multiprocessor service protocol.
 * docs: http://www.uefi.org/sites/default/files/resources/Plugfest_Multiprocessing-with_UEFI-McDaniel.pdf
 * value is from https://github.com/tianocore/edk2/blob/dff49edc2b9526a721a4fa863bbbf6e4977b58cb/MdePkg/MdePkg.dec#L969
 */
/* #define efi_mp_service_protocol_guid { \ */
/*     0x3fdda605, 0xa76e, 0x4f46, \ */
/*     { 0xad, 0x29, 0x12, 0xf4, 0x53, 0x1b, 0x3d, 0x08 } \ */
/* } */


#endif
