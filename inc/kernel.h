#include <inttypes.h>

void    k_shutdown(void);
void    k_panic(char16 *);
void    k_print(char16 *);
char16  k_readkey(void);
void    k_main(struct _loader *);
void    k_memset(void *pt, char v, uint64_t size);
void    k_salloc();
void   *k_alloc(uint64_t size);


#define OBJSIZ 2
struct _pool {
    char nused;
    struct {
        uint64_t size;
        void *start;
        char used, frontier; /* frontier tells us we need to split */
    } obj[OBJSIZ];
    struct _pool *next;
};

struct _kernel {
    struct _pool *pool;
};

/* lexer */
extern struct _lexer lexer;

static void fillbuf();
char16 getc();
void getnext();

