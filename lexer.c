#include "inc/kernel.h"

#define BUFLEN 512

struct _lexer {
    char16 *lnbuf;
    int pos;
} lexer;

static void fillbuf()
{
    if (!lexer.lnbuf)
        lexer.lnbuf = k_alloc(BUFLEN);

    int i = -1;
    do {
        lexer.lnbuf[++i] = k_readkey();
    } while (i < BUFLEN && lexer.lnbuf[i] != '\r'
                        && lexer.lnbuf[i] != '\n');
    lexer.pos = 0;
}

char16 getc()
{
    return lexer.pos;
}

void getnext()
{
    if (++lexer.pos == BUFLEN)
        fillbuf();
}
