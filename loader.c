#include "inc/efibind.h"
#include "inc/loader.h"
#include "inc/kernel.h"

/* Ease of use macros */
/* Also, what the fuck is uefi doing with their line lengths. Seriously */
#define clear_screen()                                                  \
        uefi_call_wrapper(loader.system_table->ConOut->ClearScreen, 1,  \
                          loader.system_table->ConOut)

#define print(s)                                                        \
        uefi_call_wrapper(loader.system_table->ConOut->OutputString, 2, \
                          loader.system_table->ConOut, s)

#define getkey()                                                        \
        uefi_call_wrapper(loader.system_table->ConIn->ReadKeyStroke, 2, \
                          loader.system_table->ConIn, &loader.key)

#define exit()                                                          \
        uefi_call_wrapper(loader.boot->Exit, 4,                         \
                          loader.image_handle, efi_success, 0, NULL)

#define alloc(size, pointer)                                            \
        uefi_call_wrapper(loader.boot->AllocatePool, 3,                 \
                          EfiLoaderData, size, pointer)
/* EFI_MEMORY_TYPE = EfiLoaderData because default. UEFI 2.6 p150 */

#define free(buffer)                                                    \
        uefi_call_wrapper(loader.boot->FreePool, 1, buffer)

#define getmmap(size, map, key, desc_size, desc_ver)                    \
        uefi_call_wrapper(loader.boot->GetMemoryMap, 5,                 \
                          size, map, key, desc_size, desc_ver)

/* "LocateHandleBuffer -- best name I could think of for it :( */
#define locatehbuf(type, protocol, key, nhandles, buf)                  \
        uefi_call_wrapper(loader.boot->LocateHandleBuffer, 5,           \
                          type, protocol, key, nhandles, buf)

#define oprot(handle, prot, inter)                                      \
        uefi_call_wrapper(loader.boot->OpenProtocol, 5,                 \
                          handle, prot, inter, loader.image_handle,     \
                          NULL, EFI_OPEN_PROTOCOL_BY_HANDLE_PROTOCOL)

#define qmode(mode, size, info)                                         \
        uefi_call_wrapper(loader.gfx.interface->QueryMode, 4,           \
                          loader.gfx.interface, mode, size, info)

#define setmode(mode)                                                   \
        uefi_call_wrapper(loader.gfx.interface->SetMode, 2,             \
                          loader.gfx.interface, mode)

struct _loader loader;

static void l_panic(char16 *s)
{
    print(L"\r\nLoader panic: ");
    print(s);
    print(L"\r\nPress any key to reset.");
    while (efi_not_ready == (getkey()))
        ;
    exit();
}

static void l_pdigit(uintn n)
{
    /* Yes this is hacky. Yes this is technically incorrect */
    uintn array[32] = {0};
    uintn i = 31;
    while (n > 0 && i >= 0) {
        array[i--] = n % 10;
        n /= 10;
    }
    for (i = 0; i < 31; i++)
        if (array[i] != 0)
            break;

    for (; i < 32; i++) {
        switch (array[i]) {
            case 0:    print(L"0"); break;
            case 1:    print(L"1"); break;
            case 2:    print(L"2"); break;
            case 3:    print(L"3"); break;
            case 4:    print(L"4"); break;
            case 5:    print(L"5"); break;
            case 6:    print(L"6"); break;
            case 7:    print(L"7"); break;
            case 8:    print(L"8"); break;
            case 9:    print(L"9"); break;
        }
    }
}

static void l_getgfx()
{
    /* First we have to get a protocol handle to the graphics driver,
     * then we use that to get the graphics interface
     * (i.e. the EFI_GRAPHICS_OUTPUT_PROTOCOL buffer)
     */
    efi_status state;

    /* LocateHandleBuffer : UEFI 2.6 p203
     * "Retrieves the list of handles from the handle database that meet
     *  the search criteria."
     */
    uintn count;
    efi_handle *handle_buf;
    efi_guid guid = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;

    state = locatehbuf(ByProtocol, &guid, NULL, &count, &handle_buf);
    if (state == efi_not_found)
        l_panic(L"Unable to locate graphics handle. Fatal\r\n");
    else if (state == efi_out_of_resources)
        l_panic(L"Out of memory to find graphics handle. Fatal\r\n");
    /* There is no point handling efi_invalid_parameter because we create
     * the objects right before the call. */

    print(L"Located protocol buffer\r\n");

    /* Next we open the protocol interface from the first handle returned
     * by locatehbuf */
    state = oprot(handle_buf[0], &guid, &loader.gfx.interface);
    if (state == efi_unsupported)
        l_panic(L"Graphics protocol unsupported. Fatal\r\n");
    else if (state == efi_invalid_parameter)
        l_panic(L"Incorrect argument to oprot. Fatal\r\n");

    print(L"Successfully opened the graphics protocol interface\r\n");
}

static void l_findmode()
{
    uintn mode = 0;
    uintn tmode = 0;
    uintn tmodeset = 0;
    efi_status state;
    uintn info_size;
    efi_graphics_output_mode_information *info;

    while (efi_success == (qmode(mode, &info_size, &info))) {
        l_pdigit(mode); print(L": ");
        l_pdigit(info->HorizontalResolution); print(L"x");
        l_pdigit(info->VerticalResolution); print(L"\t");
        if (++mode % 4 == 0)
            print(L"\r\n");

        if (info->HorizontalResolution == target_width &&
            info->VerticalResolution == target_height) {
            print(L"Found target resolution.\r\n");
            tmode = mode;
            tmodeset = 1;
        }
    }
    print(L"\r\nFinished discovering resolutions.\r\n");
    if (tmodeset) {
        print(L"Target resolution available, setting...\r\n");
        state = setmode(tmode);
        if (state != efi_success)
            print(L"Unable to set target resolution.\r\n");
        else
            print(L"Successfully set target resolution.\r\n");
    }
}

efi_status efiapi
efi_main (efi_handle img_handle, efi_system_table *sys_table)
{
    efi_status status;
    int setup_map_count = 0;

    InitializeLib(img_handle, sys_table);
    loader.image_handle = img_handle;
    loader.system_table = sys_table;
    loader.runtime = sys_table->RuntimeServices;
    loader.boot    = sys_table->BootServices;

    print(L"Getting screen information...\r\n");
    l_getgfx();

    print(L"\r\nFinding available resolutions...\r\n");
    l_findmode();

    loader.mem.size = 2000000000;
    status = alloc(loader.mem.size, &loader.mem.pool);

    print(L"Loading kernel...\r\n");
    k_main(&loader);

    return efi_success;
}
